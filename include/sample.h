/* 
 * File:   sample.h
 * Author: Alexandre Millecamps
 * Description: defines a set of structures and functions to handle samples
 */

#ifndef SAMPLE_H
#define	SAMPLE_H

#include <GenericTypeDefs.h>

#define SAMPLE_BUFFER_CAPACITY 1024

/**
 * Structure representing a sample
 */
typedef struct _SAMPLE {
    UINT8 channel; // ADC channel from which a sample was retrieved
    UINT16 value;  // ADC value
} SAMPLE;


/**
 * Structure representing a buffer containing samples
 */
typedef struct {
        size_t read;                             // Read position in the buffer
        size_t write;                            // Write position in the buffer
        SAMPLE buffer[ SAMPLE_BUFFER_CAPACITY ]; // Buffer containing samples
} SAMPLE_BUFFER;

/*
 * Functions
 */
inline SAMPLE SAMPLE_BUFFER_REMOVE(SAMPLE_BUFFER * queue);
inline void   SAMPLE_BUFFER_ADD(SAMPLE_BUFFER *queue, SAMPLE element);
inline short  SAMPLE_BUFFER_EMPTY(SAMPLE_BUFFER *queue);
inline short  SAMPLE_BUFFER_FULL(SAMPLE_BUFFER *queue);
       void   SAMPLE_BUFFER_INIT(SAMPLE_BUFFER *queue);

#endif	/* SAMPLE_H */

