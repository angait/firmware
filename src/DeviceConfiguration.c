#include "DeviceConfiguration.h"

/***************************************
 *  Configuration bits for the device. *
 ***************************************/
_CONFIG2(FNOSC_PRIPLL & POSCMOD_HS & PLL_96MHZ_ON & PLLDIV_DIV2) // Primary HS OSC with PLL, USBPLL /2
_CONFIG1(JTAGEN_OFF & FWDTEN_OFF & ICS_PGx2)   // JTAG off, watchdog timer off

/*!
 * Initializes Timer 3.
 */
void init_timer3(void)
{
/* On the PIC24F, if usb is enabled and the output of the PLL clock is selected
 * as the 1:1 CPU Peripheral System Clock, the system then divides this by two
 * for the instruction clock cycle Fcy */
    T3CONbits.TON = 0;       // Turn the timer off for configuration
    T3CONbits.TCS = 0; // Set the timer clock source to Fosc/2

    /* Timing configuration */
    /* NOTE ADK F_OSC = 4e6*/
    /* Timer frequency = Fosc/(2*TCKPS_RATIO*PR3) */
    /* For automatic ADC sampling
     * Sample rate = (Timer Freq)/(# channels) */
    T3CONbits.TCKPS =  0x1; // Set the prescaler mode for Timer2 to (Fosc/2)/(8)
    //T3CONbits.TCKPS =  0x3; // Set the prescaler mode for Timer2 to (Fosc/2)/(8)
    //PR3 = 255; // Sets timer match on 22.7272... kHz
//    PR3 = 255; // Sets timer match on 22.7272... kHz
//    PR3 = 145;
    PR3 = 127;
    //OSCCONbits.SOSCEN = 1; // Enable the secondary oscillator
    /* Interrupt feeding */
    IFS0bits.T3IF = 0; // Clear the Timer5 interrupt flag
    // IEC1bits.T4IE = 1; // Enable the Timer32 interrupt
    // IPC2bits.T3IP = 7; // Set timer interrupt to maximum priority
    T3CONbits.TON = 1; // Turn the timer back on
}

/*!
 * Initializes the ADC.
 */
void init_adc1(void)
{
    AD1CON1bits.ADON = 0; //Disable the ADC while performing configuration
    AD1PCFGL = 0x0; // Set A/D Port Configuration Register Low to use pins
                         // as analog pins (0) or digital (1) (range [0-65535])
    AD1PCFGH = 0x0; // Set A/D Port Configuration Register High to use pins
                         // as analog pins (0) or digital (1) (range [0-3])
    //AD1CSSL = 0xF; // Select all 16 channels for sampling
    AD1CSSL = 0B1010000100011111; // Select 7 channels for sampling
    AD1CSSL = 0;
    AD1CSSLbits.CSSL0 = 1; // Select AN0 (Respiratory Belt)
    AD1CSSLbits.CSSL1 = 1; // Select AN1 (Electro Cardiogram)
    AD1CSSLbits.CSSL2 = 1; // Select AN2 (Electro Dermal Response)
    AD1CSSLbits.CSSL3 = 1; // Select AN3 (Foot Switch Right Front)
    AD1CSSLbits.CSSL4 = 1; // Select AN4 (Foot Switch Right Back)
    AD1CSSLbits.CSSL5 = 1; // Select AN5 (Foot Switch Left Front)
    AD1CSSLbits.CSSL8 = 1; // Select AN8 (Foot Switch Left Back)

    AD1CON2bits.VCFG = 0x0; // Use the internal voltage references instead of pins
    //AD1CON1bits.SSRC = 0x0; // Set & clear SAMP to trigger sampling
    AD1CON1bits.SSRC = 2; // Use Timer3 compare match to end sampling process
    //AD1CON3bits.SAMC = 0b10000; // Auto-sample time (in T_AD cycles [0-31])
    AD1CON1bits.FORM = 0b00; // Set data output to integer format
    AD1CON2bits.SMPI = 0x8; // Set 8 samples per interrupt
    AD1CON2bits.CSCNA = 1; // Enable channel scanning on MUX A
    AD1CON2bits.BUFM = 1; // 0 = Enable buffer as one 16-word buffer
                          // 1 = two 8-word buffers
    AD1CON2bits.ALTS = 0; // Do not alternate between MUX A & B upon successive
                          //conversions
    AD1CON3bits.ADRC = 0; // Do not use internal RC clock as T_AD source, use T_CY
    // Note Tcy = 1/Fcy => 4*Fosc = 4*8e6 in this case
    AD1CON3bits.ADCS = 10; // A/D Conversion Clock Period Select (Tad = (1+ADCS)/(2*Fcy)

    //AD1CHSbits.CH0NA = 0; //MUXA uses internal negative voltage reference
    //AD1CHSbits.CH0NB = 0; //MUXB uses internal negative voltage references
    //AD1CHSbits.CH0SA = 0; //MUXA uses AN0 as positive voltage channel
    //AD1CHSbits.CH0SB = 0; //MUXB uses AN0 as positive voltage channel
    AD1CON1bits.ADSIDL = 0; // Do not stop A/D Module if idle
    AD1CON1bits.ASAM = 1; // Enable automatic sampling after conversion
    IFS0bits.AD1IF = 0; // Clear the Interrupt Flag for AD1
    IPC3bits.AD1IP = 6;
    AD1CON1bits.ADON = 1; // Turn the ADC module back on affter configuration
}

/*!
 * Initializes the SPI module in order to comunicate with the accelerometer.
 */
void init_accel(void)
{
//    // Set pins to normal digital operation
//    ODCDBITS.ODD9 = 0;
//    ODCDBITS.ODD10 = 0;
//    ODCDBITS.ODD11 = 0;
//
//    /* The accelerometer is a parallax H48C and uses a three wire interface as
//     * detailed in the spec sheet for MCP 3204/3208. However, three wire spi
//     * is not supported by most PICs, a "bit-banging" technique is used to
//     * simulate spi through the general purpose data input/output ports */
//    TRISDbits.TRISD9 = 1; // Set D9 as the data i/o for the h48c parallax module
//    TRISDbits.TRISD10 = 0; // Set D10 as the master clock to drive the h48c
//    TRISDbits.TRISD11 = 0; // Set D11 as the slave select line
    
    /* Configure remappable pins to use SPI */

    // Unlock OSCCON register and set IOLOCK to 0
    __builtin_write_OSCCONL(OSCCON & 0xBF);

        // Associate RP4 to "SPI1 Data Input"
        RPINR20bits.SDI1R = 4;

        // Associate RP2 to "SPI1 Data Output"
        RPOR1bits.RP2R = 7;

        // Associate RP3 to "SPI1 Clock Output"
        RPOR1bits.RP3R = 8;

        // Associate RP12 to "Slave Select Output"
        RPOR6bits.RP12R = 9;

    // Unlock OSCCON register and set IOLOCK to 1
    __builtin_write_OSCCONL(OSCCON | 0x40);

     // Set slave select as output
    //TRISDbits.TRISD11 = 0;
    // Slave select high by default
    //LATDbits.LATD11 = 1;

    /* Configure the SPI module */
    SPI1STATbits.SPIEN =  0; // Disable the SPI module
    
    SPI1CON1bits.MSTEN  = 1; // Enable master mode
    SPI1CON1bits.MODE16 = 0; // Enable 8-bit mode of operation
    SPI1CON2bits.SPIBEN = 1; // Enable enhanced buffer mode

    // Configure Frame Master mode //
    
    SPI1CON2bits.FRMEN   = 1; // Enable framed SPI operation
    SPI1CON2bits.SPIFSD  = 0; // Frame sync pulse direction (output)
    SPI1CON2bits.SPIFPOL = 0; // Sync signal active-low
    SPI1CON2bits.SPIFE   = 0; // Frame sync precedes data transmission
    
    
    // Set the SPI clock frequency to 667 kHz
    SPI1CON1bits.PPRE = 0B10;  // Set the primary SPI clock prescaler to 4:1
    SPI1CON1bits.SPRE = 0B010; // Set the secondary SPI clock prescaler 6:1
    SPI1CON1bits.SMP = 0;      // Data input sample phase:
                               // input data sampled at end of data output time
    
    SPI1CON1bits.CKP = 0;      // Latch data on rising edge, receive data on
                               // falling edge of clock

    SPI1CON1bits.CKE = 0;      // Not used in Frame mode but need to be cleared
    //SPI1CON1bits.DISSDO = 0; // Enable  MOSI pin
    //SPI1CON1bits.SSEN = 1;
    

    SPI1STATbits.SISEL = 0B101; // Interrupt when the last bit is shifted out of
                                // SPIxSR, now the transmit is complete

    // Clear receive overflow flag //
    SPI1STATbits.SPIROV = 0;

    // Activate interrupts //
    IFS0bits.SPI1IF = 0; // Clear the interrupt flag
    IPC2bits.SPI1IP = 7; // Set SPI interrupt to highest priority
    IEC0bits.SPI1IE = 1; // Enable the interrupt for the SPI module

    SPI1STATbits.SPIEN = 1; // Enable SPI module

    // Map pins for SPI1

//    RPINR20bits.SDI1R = 4; // Set SPI1 input to RP4
//    TRISDbits.TRISD8  = 0; // Set pin RP2 (RD8) to output (MISO)
//    TRISDbits.TRISD9  = 0; // Set pin RP4 (RD9) to output (M0SI)
//    RPOR2bits.RP4R    = 7; // Set RP2 to SDI1 data output

    //TRISDbits.TRISD9  = 1; // Set RP2 to SDI1 data input (MISO)
    //RPINR20bits.SDI1R = 2;
    //TODO figure out why TRISD8 is unable to read and only reads the SPI outputs?
    
    // All that is wrong! To be deleted
    /*
    TRISDbits.TRISD8  = 0; // Set pin RP2 (RD8) to output (MOSI)
    RPOR1bits.RP2R    = 7;
    //TRISFbits.TRISF8 = 1; // Set pin RP15 (RF8) to input (MISO)
    RPINR20bits.SDI1R = 15;

    //TRISFbits.TRISF2   = 0; // Set pin RP30 (RF2) to output
    RPOR15bits.RP30R   = 8; // Set pin RP30 to SCK
    */
    /*
    TRISAbits.TRISA15  = 0; // Set RA15 to output for slave select
    LATAbits.LATA15    = 1; // Set slave select high as it's active low
    SPI1STATbits.SPIEN = 1; // Enable the SPI module
    */
}