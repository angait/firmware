/* 
 * File:   DeviceConfiguration.h
 * Author: Alexandre Millecamps
 * Description: Device configuration is made here
 */

#ifndef DEVICECONFIGURATION_H
#define	DEVICECONFIGURATION_H

#include <Compiler.h>
#include <GenericTypeDefs.h>

#define CLOCK_FREQ 32000000

// If a maximum current rating hasn't been defined, then define 500mA by default
#ifndef MAX_ALLOWED_CURRENT
    #define MAX_ALLOWED_CURRENT             (500)         // Maximum power we can supply in mA
#endif

// Allows debug print interface
#define DEBUG_Init(a)
#define DEBUG_Error(a)
#define DEBUG_PrintString(a)
#define DEBUG_PrintHex(a)
#define GetSystemClock() (CLOCK_FREQ)
#define GetInstructionClock() (GetSystemClock() / 2)

/**************************************
 * Peripheral Initilization functions *
 **************************************/
void init_timer3(void);
void init_adc1(void);
void init_accel(void);

#endif	/* DEVICECONFIGURATION_H */

