/* 
 * File:   DeviceCommunication.h
 * Author: Alexandre Millecamps
 *
 * Functions related to the communication with the phone are implemented here.
 *
 */

#ifndef DEVICECOMMUNICATION_H
#define	DEVICECOMMUNICATION_H

#include <USB/usb.h>
#include <USB/usb_host_android.h>

#include "DeviceConfiguration.h"

static void* device_handle = NULL;   // Pointer to the connected device
static BOOL device_attached = FALSE; // Device attachment flag

inline BOOL android_sent_command(const char command);

#endif	/* DEVICECOMMUNICATION_H */

