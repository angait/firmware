/*******************************************************************************

    USB Android Accessory basic demo with accessory in host mode

*******************************************************************************/
//DOM-IGNORE-BEGIN
/******************************************************************************

FileName:        main.c
Dependencies:    None
Processor:       PIC24/dsPIC30/dsPIC33/PIC32MX
Compiler:        C30/C32
Company:         Microchip Technology, Inc.

Software License Agreement

The software supplied herewith by Microchip Technology Incorporated
(the "Company") for its PICmicro(R) Microcontroller is intended and
supplied to you, the Company�s customer, for use solely and
exclusively on Microchip PICmicro Microcontroller products. The
software is owned by the Company and/or its supplier, and is
protected under applicable copyright laws. All rights are reserved.
Any use in violation of the foregoing restrictions may subject the
user to criminal sanctions under applicable laws, as well as to
civil liability for the breach of the terms and conditions of this
license.

THIS SOFTWARE IS PROVIDED IN AN �AS IS� CONDITION. NO WARRANTIES,
WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED
TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE APPLY TO THIS SOFTWARE. THE COMPANY SHALL NOT,
IN ANY CIRCUMSTANCES, BE LIABLE FOR SPECIAL, INCIDENTAL OR
CONSEQUENTIAL DAMAGES, FOR ANY REASON WHATSOEVER.

Change History
  Rev      Description
  -----    ----------------------------------
  2.9      Initial revision
*******************************************************************************/

/////////////////////DEBUG/////////////////////////////////////////////////////

#include <GenericTypeDefs.h>
#include <stdint.h>
#include <stddef.h>

typedef struct _SAMPLE {
    uint8_t channel;
    uint16_t value;
} SAMPLE;

#define SAMPLE_BUFFER_CAPACITY 1024
//#define SAMPLE_BUFFER_SIZE 65

//#include "ring_buffer.h"
//RING_BUFFER(SAMPLE,SAMPLE_BUFFER,SAMPLE_BUFFER_CAPACITY)

#include "atomic_struct_queue.h"
AtomicStructQueueT(SAMPLE, SAMPLE_BUFFER, SAMPLE_BUFFER_CAPACITY);


// Include files
#include <timer.h>
#include "USB/usb.h"
#include "USB/usb_host_android.h"
#include "Compiler.h"
#include "HardwareProfile.h"
#include "USB/usb_hal_pic24f.h"

// If a maximum current rating hasn't been defined, then define 500mA by default
#ifndef MAX_ALLOWED_CURRENT
    #define MAX_ALLOWED_CURRENT             (500)         // Maximum power we can supply in mA
#endif

// Configuration bits for the device.  Please refer to the device datasheet for each device
//   to determine the correct configuration bit settings
#ifdef __C30__
    #if defined(__PIC24FJ256GB110__)
        _CONFIG2(FNOSC_PRIPLL & POSCMOD_HS & PLL_96MHZ_ON & PLLDIV_DIV2) // Primary HS OSC with PLL, USBPLL /2
        _CONFIG1(JTAGEN_OFF & FWDTEN_OFF & ICS_PGx2)   // JTAG off, watchdog timer off

    #elif defined(__PIC24FJ64GB004__)
        _CONFIG1(WDTPS_PS1 & FWPSA_PR32 & WINDIS_OFF & FWDTEN_OFF & ICS_PGx1 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)
        _CONFIG2(POSCMOD_HS & I2C1SEL_PRI & IOL1WAY_OFF & OSCIOFNC_ON & FCKSM_CSDCMD & FNOSC_PRIPLL & PLL96MHZ_ON & PLLDIV_DIV2 & IESO_ON)
        _CONFIG3(WPFP_WPFP0 & SOSCSEL_SOSC & WUTSEL_LEG & WPDIS_WPDIS & WPCFG_WPCFGDIS & WPEND_WPENDMEM)
        _CONFIG4(DSWDTPS_DSWDTPS3 & DSWDTOSC_LPRC & RTCOSC_SOSC & DSBOREN_OFF & DSWDTEN_OFF)
    #elif defined(__PIC24FJ64GB502__)
        _CONFIG1(WDTPS_PS1 & FWPSA_PR32 & WINDIS_OFF & FWDTEN_OFF & ICS_PGx1 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)
        _CONFIG2(I2C1SEL_PRI & IOL1WAY_OFF & FCKSM_CSDCMD & FNOSC_PRIPLL & PLL96MHZ_ON & PLLDIV_DIV2 & IESO_OFF)
        _CONFIG3(WPFP_WPFP0 & SOSCSEL_SOSC & WUTSEL_LEG & WPDIS_WPDIS & WPCFG_WPCFGDIS & WPEND_WPENDMEM)
        _CONFIG4(DSWDTPS_DSWDTPS3 & DSWDTOSC_LPRC & RTCOSC_SOSC & DSBOREN_OFF & DSWDTEN_OFF)
    #elif defined(__PIC24FJ256GB106__)
        _CONFIG1( JTAGEN_OFF & GCP_OFF & GWRP_OFF & COE_OFF & FWDTEN_OFF & ICS_PGx2)
        _CONFIG2( 0xF7FF & IESO_OFF & FCKSM_CSDCMD & OSCIOFNC_OFF & POSCMOD_HS & FNOSC_PRIPLL & PLLDIV_DIV3 & IOL1WAY_ON)
    #elif defined(__PIC24FJ256DA210__) || defined(__PIC24FJ256GB210__)
        _CONFIG1(FWDTEN_OFF & ICS_PGx2 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)
        _CONFIG2(POSCMOD_HS & IOL1WAY_ON & OSCIOFNC_ON & FCKSM_CSDCMD & FNOSC_PRIPLL & PLL96MHZ_ON & PLLDIV_DIV2 & IESO_OFF)
    #elif defined(IOIO)
        _CONFIG1(FWDTEN_OFF & ICS_PGx2 & GWRP_OFF & GCP_OFF & JTAGEN_OFF)
        _CONFIG2(POSCMOD_NONE & IOL1WAY_ON & OSCIOFNC_ON & FCKSM_CSDCMD & FNOSC_FRCPLL & PLL96MHZ_ON & PLLDIV_NODIV & IESO_OFF)
    #elif defined(__dsPIC33EP512MU810__) || defined(PIC24EP512GU810_PIM)
        _FOSCSEL(FNOSC_FRC);
        _FOSC(FCKSM_CSECMD & OSCIOFNC_OFF & POSCMD_XT);
        _FWDT(FWDTEN_OFF);
    #endif
#elif defined( __PIC32MX__ )
    #pragma config UPLLEN   = ON            // USB PLL Enabled
    #pragma config FPLLMUL  = MUL_15        // PLL Multiplier
    #pragma config UPLLIDIV = DIV_2         // USB PLL Input Divider
    #pragma config FPLLIDIV = DIV_2         // PLL Input Divider
    #pragma config FPLLODIV = DIV_1         // PLL Output Divider
    #pragma config FPBDIV   = DIV_1         // Peripheral Clock divisor
    #pragma config FWDTEN   = OFF           // Watchdog Timer
    #pragma config WDTPS    = PS1           // Watchdog Timer Postscale
    //#pragma config FCKSM    = CSDCMD        // Clock Switching & Fail Safe Clock Monitor
    #pragma config OSCIOFNC = OFF           // CLKO Enable
    #pragma config POSCMOD  = HS            // Primary Oscillator
    #pragma config IESO     = OFF           // Internal/External Switch-over
    #pragma config FSOSCEN  = OFF           // Secondary Oscillator Enable (KLO was off)
    #pragma config FNOSC    = PRIPLL        // Oscillator Selection
    #pragma config CP       = OFF           // Code Protect
    #pragma config BWP      = OFF           // Boot Flash Write Protect
    #pragma config PWP      = OFF           // Program Flash Write Protect
    #pragma config ICESEL   = ICS_PGx2      // ICE/ICD Comm Channel Select
    //#pragma config DEBUG    = ON            // Background Debugger Enable

#else
    #error Cannot define configuration bits.
#endif

// C30 and C32 Exception Handlers
// If your code gets here, you either tried to read or write
// a NULL pointer, or your application overflowed the stack
// by having too many local variables or parameters declared.
#if defined(__C30__)
	void _ISR __attribute__((__no_auto_psv__)) _AddressError(void)
	{
        while(1){}
	}
	void _ISR __attribute__((__no_auto_psv__)) _StackError(void)
	{
        while(1){}
	}

#elif defined(__C32__)
	void _general_exception_handler(unsigned cause, unsigned status)
	{
        #if defined(DEBUG_MODE)
            unsigned long address = _CP0_GET_EPC();
        #endif

        while(1){}
	}
#endif

//Local prototypes
#if defined(__C32__)
static void InitPIC32(void);
#endif

static void* device_handle = NULL;
static BOOL device_attached = FALSE;

static char manufacturer[] = "University of Pittsburgh";
static char model[] = "angait";
static char description[] = "Angait: An Android Sampling Platform";
static char version[] = "0.1";
static char uri[] = "http://www.imedlab.org/";
static char serial[] = "N/A";

ANDROID_ACCESSORY_INFORMATION myDeviceInfo =
{
    manufacturer,
    sizeof(manufacturer),
    model,
    sizeof(model),
    description,
    sizeof(description),
    version,
    sizeof(version),
    uri,
    sizeof(uri),
    serial,
    sizeof(serial)
};


/*****************************
 * Peripheral Initilizations *
 *****************************/
void init_timer3(void)
{

    T3CONbits.TON = 0;       // Turn the timer off for configuration
    T3CONbits.TCS = 0; // Set the timer clock source to Fosc/2

    /* Timing configuration */
    /* NOTE ADK F_OSC = 4e6*/
    /* Timer frequency = Fosc/(2*TCKPS_RATIO*PR3) */
    /* For automatic ADC sampling
     * Sample rate = (Timer Freq)/(# channels) */
    T3CONbits.TCKPS =  0x1; // Set the prescaler mode for Timer2 to (Fosc/2)/(8)
    //T3CONbits.TCKPS =  0x3; // Set the prescaler mode for Timer2 to (Fosc/2)/(8)
    //PR3 = 255; // Sets timer match on 22.7272... kHz
//    PR3 = 145; // Sets timer match on 22.7272... kHz
    PR3 = 255;
    //PR3 = 255;
    //OSCCONbits.SOSCEN = 1; // Enable the secondary oscillator
    /* Interrupt feeding */
    IFS0bits.T3IF = 0; // Clear the Timer5 interrupt flag
    // IEC1bits.T4IE = 1; // Enable the Timer32 interrupt
    // IPC2bits.T3IP = 7; // Set timer interrupt to maximum priority
    T3CONbits.TON = 1; // Turn the timer back on
}

void init_adc1(void)
{
    AD1CON1bits.ADON = 0; //Disable the ADC while performing configuration
    AD1PCFGL = 0x0; // Set A/D Port Configuration Register Low to use pins
                         // as analog pins (0) or digital (1) (range [0-65535])
    AD1PCFGH = 0x0; // Set A/D Port Configuration Register High to use pins
                         // as analog pins (0) or digital (1) (range [0-3])
    //AD1CSSL = 0xF; // Select all 16 channels for sampling
    AD1CSSL = 0x611F; // Select 7 channels for sampling
    AD1CON2bits.VCFG = 0x0; // Use the internal voltage references instead of pins
    //AD1CON1bits.SSRC = 0x0; // Set & clear SAMP to trigger sampling
    AD1CON1bits.SSRC = 2; // Use Timer3 compare match to end sampling process
    //AD1CON3bits.SAMC = 0b10000; // Auto-sample time (in T_AD cycles [0-31])
    AD1CON1bits.FORM = 0b00; // Set data output to integer format
    AD1CON2bits.SMPI = 0x8; // Set 8 samples per interrupt
    AD1CON2bits.CSCNA = 1; // Enable channel scanning on MUX A
    AD1CON2bits.BUFM = 1; // 0 = Enable buffer as one 16-word buffer
                          // 1 = two 8-word buffers
    AD1CON2bits.ALTS = 0; // Do not alternate between MUX A & B upon successive
                          //conversions
    AD1CON3bits.ADRC = 0; // Do not use internal RC clock as T_AD source, use T_CY
    // Note Tcy = 1/Fcy => 4*Fosc = 4*8e6 in this case
    AD1CON3bits.ADCS = 10; // A/D Conversion Clock Period Select (Tad = (1+ADCS)/(2*Fcy)

    //AD1CHSbits.CH0NA = 0; //MUXA uses internal negative voltage reference
    //AD1CHSbits.CH0NB = 0; //MUXB uses internal negative voltage references
    //AD1CHSbits.CH0SA = 0; //MUXA uses AN0 as positive voltage channel
    //AD1CHSbits.CH0SB = 0; //MUXB uses AN0 as positive voltage channel
    AD1CON1bits.ADSIDL = 0; // Do not stop A/D Module if idle
    AD1CON1bits.ASAM = 1; // Enable automatic sampling after conversion
    IFS0bits.AD1IF = 0; // Clear the Interrupt Flag for AD1
    IPC3bits.AD1IP = 6;
    AD1CON1bits.ADON = 1; // Turn the ADC module back on affter configuration
}

void init_error_leds(void)
{
    return;
}
void init_accel(void)
{
//    // Set pins to normal digital operation
//    ODCDBITS.ODD9 = 0;
//    ODCDBITS.ODD10 = 0;
//    ODCDBITS.ODD11 = 0;
//
//    /* The accelerometer is a parallax H48C and uses a three wire interface as
//     * detailed in the spec sheet for MCP 3204/3208. However, three wire spi
//     * is not supported by most PICs, a "bit-banging" technique is used to
//     * simulate spi through the general purpose data input/output ports */
//    TRISDbits.TRISD9 = 1; // Set D9 as the data i/o for the h48c parallax module
//    TRISDbits.TRISD10 = 0; // Set D10 as the master clock to drive the h48c
//    TRISDbits.TRISD11 = 0; // Set D11 as the slave select line
    SPI1STATbits.SPIEN = 0; // Disable the SPI module

    SPI1CON1bits.MSTEN = 1; // Enable master mode
    SPI1STATbits.SPIROV = 0;
    SPI1CON1bits.MODE16 = 0; // Enable 8-bit mode of operation
    SPI1CON1bits.PPRE = 0B10; // Set the primary SPI clock prescaler to 4:1
    SPI1CON1bits.SPRE = 0B10; // Set the secondary SPI clock prescaler 6:1
    SPI1CON1bits.SMP = 0;
    SPI1CON1bits.CKP = 1;
    SPI1CON1bits.CKE = 0;
    SPI1CON1bits.DISSDO = 0; // Enable  MOSI pin
    //SPI1CON1bits.SSEN = 1;
    SPI1CON2bits.FRMEN = 0; // Disable framed spi operation
    SPI1CON2bits.SPIBEN = 1; // Enable enhanced buffer mode

    SPI1STATbits.SISEL = 0B101; // Interrupt when the last bit is shifted out of SPIxSR, now the transmit is complete

    IFS0bits.SPI1IF = 0; // Clear the interrupt flag

    IPC2bits.SPI1IP = 7; // Set SPI interrupt to highest priority
    IEC0bits.SPI1IE = 1; // Enable the interrupt for the SPI module

    // Map pins for SPI1
    
//    RPINR20bits.SDI1R = 4; // Set SPI1 input to RP4
//    TRISDbits.TRISD8  = 0; // Set pin RP2 (RD8) to output (MISO)
//    TRISDbits.TRISD9  = 0; // Set pin RP4 (RD9) to output (M0SI)
//    RPOR2bits.RP4R    = 7; // Set RP2 to SDI1 data output

    //TRISDbits.TRISD9  = 1; // Set RP2 to SDI1 data input (MISO)
    //RPINR20bits.SDI1R = 2;
    //TODO figure out why TRISD8 is unable to read and only reads the SPI outputs?
    TRISDbits.TRISD8  = 0; // Set pin RP2 (RD8) to output (MOSI)
    RPOR1bits.RP2R    = 7;
    TRISFbits.TRISF8 = 1; // Set pin RP15 (RF8) to input (MISO)
    RPINR20bits.SDI1R = 15;

    TRISFbits.TRISF2   = 0; // Set pin RP30 (RF2) to output
    RPOR15bits.RP30R   = 8; // Set pin RP30 to SCK
    
    TRISAbits.TRISA15  = 0; // Set RA15 to output for slave select
    LATAbits.LATA15    = 1; // Set slave select high as it's active low
    SPI1STATbits.SPIEN = 1; // Enable the SPI module
}

SAMPLE_BUFFER samples;
BOOL BUFFER_OVERFLOW = FALSE;
BOOL INTERRUPT_MISSED = FALSE;


typedef struct _ACCEL_AXIS {
    BYTE value;
    struct _ACCEL_AXIS * next;
} ACCEL_AXIS;

static ACCEL_AXIS X_AXIS, Y_AXIS, Z_AXIS, *currentAxis;
void init_axis(void)
{
    X_AXIS.value = 0B11000000;
    //X_AXIS.value = 0B11010100;
    X_AXIS.next = &Y_AXIS;
    Y_AXIS.value = 0B11001000;
    //Y_AXIS.value = 0B11000000;
    Y_AXIS.next = &Z_AXIS;
    Z_AXIS.value = 0B11010000;
    //Z_AXIS.value = 0B11000000;
    Z_AXIS.next = &X_AXIS;
    currentAxis = &X_AXIS;
}

void __attribute__ ((__interrupt__, no_auto_psv)) _ADC1Interrupt(void)
{
    static SAMPLE s;
    static BOOL INTERRUPT_TOGGLE = FALSE;
    static BOOL ENTERED_INTERRUPT;

    /* Ensure the accuracy of sampling by checking for missed interrupts, dangerous!*/
    INTERRUPT_TOGGLE = !INTERRUPT_TOGGLE;
    LATBbits.LATB12 = INTERRUPT_TOGGLE;
    
    if(ENTERED_INTERRUPT) {
        INTERRUPT_MISSED = TRUE;
    } else {
        IFS0bits.AD1IF = 0;
        ENTERED_INTERRUPT = TRUE;
    }

    static int i;
    static unsigned int j = 1;
    for (i = 0; i < 8; ++i)
    {
        s.channel = i;
        s.value = (&ADC1BUF0)[i+AD1CON2bits.BUFS*8];

        if (!SAMPLE_BUFFER_FULL(&samples)) {
            SAMPLE_BUFFER_ADD(&samples, s);
        } else {
            /* TODO document that sometimes dalvik cache must be cleared for this to work */
            BUFFER_OVERFLOW = TRUE;
        }
    }

    /* Send three bytes out of SPI1 to initiate communication with H48C */
    LATAbits.LATA15 = 0; // Set slave select low

    SPI1BUF = currentAxis->value;
    SPI1BUF = 0;
    SPI1BUF = 0;

    /* End missed interrupt detection */
    ENTERED_INTERRUPT = FALSE;
}

void __attribute__ ((__interrupt__, no_auto_psv)) _SPI1Interrupt(void)
{
    static BYTE byte1;
    static BYTE byte2;
    static BYTE byte3;
    static SAMPLE s;
    LATAbits.LATA15 = 1;
    byte1 = SPI1BUF;
    byte2 = SPI1BUF;
    byte3 = SPI1BUF;
    /*TODO check the count register for SPI1BUF. The above approach may be dangerous */

    /* Add sample to device*/
    s.channel = (currentAxis->value & 0B111)+16;
    s.value = (uint16_t)(byte1>>7)<<11|(uint16_t)byte2<<3|(uint16_t)byte3>>5; // Concat lsByte and msB
    if (!SAMPLE_BUFFER_FULL(&samples)) {
        SAMPLE_BUFFER_ADD(&samples, s);
    } else {
        /* TODO document that sometimes dalvik cache must be cleared for this to work */
        BUFFER_OVERFLOW = TRUE;
    }

    currentAxis = currentAxis->next;
    IFS0bits.SPI1IF = 0;
}

/* android_sent_toggle()
 * Precondition: android device is attached for reading
 * Returns: true upon a toggle command being sent from the android phone
 */

inline BOOL android_sent_command(const char command) {
    static BYTE errorCode;
    static DWORD size = 0;
    static BOOL toggleSent;
    static BYTE read_buffer[64];
    
    toggleSent = FALSE;

    if(AndroidAppIsReadComplete(device_handle, &errorCode, &size))
    {
        errorCode = AndroidAppRead(device_handle, (BYTE*)&read_buffer, (DWORD)sizeof(read_buffer));
        // If the device is attached, then lets wait for a command from the application
        if( errorCode != USB_SUCCESS)
        {
            DEBUG_PrintString("Error trying to start read");
        }
        else if (size > 0 && read_buffer[0] == command)
        {
            toggleSent = TRUE;
        }
    }

    return toggleSent;
}

// Define the output packet size in bytes
#define OUT_PKT_SZ 64

BOOL write_sample_to_device(SAMPLE s) {
    /* TODO rewrite to accomidate sample values of variable bit lengths */
    /* Assumes OUT_PKT_SZ is divisible by sizeof(s.value) */
    /* returns true write to device was successfull*/
    static BYTE fillArray[OUT_PKT_SZ];
    static BYTE sendArray[OUT_PKT_SZ];
    static DWORD fillArraySize = 0; // size of send buffer in Bytes
    
    unsigned short handledSamples;
    BYTE errorCode;
    DWORD size = 0;
    BOOL success = FALSE;
    unsigned short i;
    for (i = 0;
            i < sizeof(s.value)/sizeof(BYTE)
            && fillArraySize < 64;
            ++i)
    {
        fillArray[fillArraySize++] = (s.value)>>(8*(sizeof(s.value)-i-1));
        success = TRUE;
    }

    if (fillArraySize >= 64)
    {
        if(AndroidAppIsWriteComplete(device_handle, &errorCode, &size) \
                == TRUE) {
            size_t memcpy_size = sizeof(BYTE)*64;
            memcpy(sendArray, fillArray, memcpy_size);
            errorCode = AndroidAppWrite(device_handle,sendArray,64);

            if (errorCode == USB_ENDPOINT_BUSY) {
                /* the end point was busy */
                /* TODO return -1 so that the user knows to call this function again? */
                /* The problem with this solution is that the user would have to pass in bad parameters */
                /* The question is when does this error occur if AndroidAppWriteIsComplete is already checked? */
                /* return -1; */
                success = FALSE;
            } else if(errorCode != USB_SUCCESS)
            {
                //DEBUG_PrintString("Error trying to complete write. Resetting");
                success = FALSE;
            } else {
                /* succesfully called usb to send packet, so let's reset */
                fillArraySize = 0;
                success = TRUE;
            }
        }
    }
    return success;
}

typedef enum {SAMPLING, CLOSING, IDLE, WAITING, RESET, OVERFLOW} STATE;
STATE current_state, next_state;
BOOL lastWriteSuccess;
BYTE sendBuffer[64];
unsigned int sendBufferSize;

int main(void)
{
    #if defined(__PIC32MX__)
        InitPIC32();
    #endif

    #if defined(__dsPIC33EP512MU810__) || defined (__PIC24EP512GU810__)

    // Configure the device PLL to obtain 60 MIPS operation. The crystal
    // frequency is 8MHz. Divide 8MHz by 2, multiply by 60 and divide by
    // 2. This results in Fosc of 120MHz. The CPU clock frequency is
    // Fcy = Fosc/2 = 60MHz. Wait for the Primary PLL to lock and then
    // configure the auxilliary PLL to provide 48MHz needed for USB
    // Operation.

	PLLFBD = 38;				/* M  = 60	*/
	CLKDIVbits.PLLPOST = 0;		/* N1 = 2	*/
	CLKDIVbits.PLLPRE = 0;		/* N2 = 2	*/
	OSCTUN = 0;

    /*	Initiate Clock Switch to Primary
     *	Oscillator with PLL (NOSC= 0x3)*/

    __builtin_write_OSCCONH(0x03);
	__builtin_write_OSCCONL(0x01);
	while (OSCCONbits.COSC != 0x3);

    /* Configuring the auxiliary PLL, since the primary
     * oscillator provides the source clock to the auxiliary
     * PLL, the auxiliary oscillator is disabled. Note that
     * the AUX PLL is enabled. The input 8MHz clock is divided
     * by 2, multiplied by 24 and then divided by 2. Wait till
     * the AUX PLL locks. */

    ACLKCON3 = 0x24C1;
    ACLKDIV3 = 0x7;
    ACLKCON3bits.ENAPLL = 1;
    while(ACLKCON3bits.APLLCK != 1);

    TRISBbits.TRISB5 = 0;
    LATBbits.LATB5 = 1;

    #endif

    /************************
     * Angait initilization *
     ************************/

    SAMPLE_BUFFER_INIT(&samples);
    USBInitialize(0);
    AndroidAppStart(&myDeviceInfo);

    // initilize peripherals
    init_timer3();
    init_adc1();
    init_error_leds();
    init_accel();

    // Initialize data structures
    init_axis();
    //mInitPOT();


    DEBUG_Init(1);
    current_state = RESET;
    next_state = RESET;
    SAMPLE s;
    BUFFER_OVERFLOW = FALSE;
    InitAllLEDs();

    TRISBbits.TRISB12 = 0;
    
    for(;;)
    {
        // Set status LEDs
        mLED_4 = BUFFER_OVERFLOW;
        mLED_5 = BUFFER_OVERFLOW;
        
        // Advance the state machine
        current_state = next_state;

        //Keep the USB stack running
        USBTasks();

        switch(current_state)
        {
        case IDLE:
            if(device_attached /* && android_sent_toggle()*/) {
                // TODO: add power saving state
                next_state = WAITING;
            }

            break;
        case WAITING:
            if (device_attached)
            {
                if(android_sent_command('x'))
                {
                    // Enable the timer interrupt for sampling
                    SAMPLE_BUFFER_INIT(&samples);
                    int z;
                    // This delay was found experimentally necessary to allow for
                    // Android's buffers to adjust to getting the data rate up.
                    for(z = 0; z < INT_MAX; ++z) USBTasks();
                    INTERRUPT_MISSED = FALSE;
                    BUFFER_OVERFLOW = FALSE;
                    IEC0bits.SPI1IE = 1;
                    IEC0bits.AD1IE  = 1;
                    next_state = SAMPLING;
                }
            } else {
                next_state = IDLE;
            }
            break;

        case SAMPLING:
            if(BUFFER_OVERFLOW || android_sent_command('s') || !device_attached)
            {
                next_state = CLOSING;
            }
            if(!SAMPLE_BUFFER_EMPTY(&samples))
            {
                if(lastWriteSuccess > 0) {
                    s = SAMPLE_BUFFER_REMOVE(&samples);
                }
                lastWriteSuccess = write_sample_to_device(s); //TODO add reset for errors
            }
            break;

        case CLOSING:
            //TODO: See android issue 20545 for the necessity of this.
            if(write_sample_to_device(s)) { // TODO fix this to write a last sample
                next_state = RESET;
            }
            IEC0bits.AD1IE = 0;
            IEC0bits.SPI1IE = 0;
            break;

        case OVERFLOW:
            next_state = CLOSING;
            IEC0bits.AD1IE = 0;
            IEC0bits.SPI1IE = 0;
            if(android_sent_command('x') || !device_attached)
            {
                IEC0bits.AD1IE = 0;
                next_state = CLOSING;
            } else {
                 if(!SAMPLE_BUFFER_EMPTY(&samples))
                 {
                    if(lastWriteSuccess) {
                        s = SAMPLE_BUFFER_REMOVE(&samples);
                    }
                    lastWriteSuccess = write_sample_to_device(s); //TODO add reset for errors
                }
            }
            break;
        case RESET:
            DEBUG_PrintString("Entering RESET");
            next_state = IDLE;
            // Disable the timer interrupt for sampling
            IEC0bits.AD1IE = 0;
            IEC0bits.SPI1IE = 0;
            
            break;
        }
    }
}


/****************************************************************************
  Function:
    BOOL USB_ApplicationDataEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )

  Summary:
    Handles USB data application events

  Description:
    Handles USB data application events

  Precondition:
    None

  Parameters:
    BYTE address - address of the device causing the event
    USB_EVENT event - the event that has occurred
    void* data - data associated with the event
    DWORD size - the size of the data in the data field

  Return Values:
    BOOL - Return TRUE of the event was processed.  Return FALSE if the event
           wasn't handled.

  Remarks:
    None
  ***************************************************************************/
BOOL USB_ApplicationDataEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )
{
    return FALSE;
}


/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )

  Summary:
    Handles USB application events

  Description:
    Handles USB application events

  Precondition:
    None

  Parameters:
    BYTE address - address of the device causing the event
    USB_EVENT event - the event that has occurred
    void* data - data associated with the event
    DWORD size - the size of the data in the data field

  Return Values:
    BOOL - Return TRUE of the event was processed.  Return FALSE if the event
           wasn't handled.

  Remarks:
    None
  ***************************************************************************/
BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )
{
    switch( (INT)event )
    {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            if (((USB_VBUS_POWER_EVENT_DATA*)data)->current <= (MAX_ALLOWED_CURRENT / 2))
            {
                return TRUE;
            }
            else
            {
                DEBUG_PrintString( "\r\n***** USB Error - device requires too much current *****\r\n" );
            }
            break;

        case EVENT_VBUS_RELEASE_POWER:
        case EVENT_HUB_ATTACH:
        case EVENT_UNSUPPORTED_DEVICE:
        case EVENT_CANNOT_ENUMERATE:
        case EVENT_CLIENT_INIT_ERROR:
        case EVENT_OUT_OF_MEMORY:
        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
        case EVENT_DETACH:                   // USB cable has been detached (data: BYTE, address of device)
        case EVENT_ANDROID_DETACH:
            device_attached = FALSE;
            return TRUE;
            break;

        // Android Specific events
        case EVENT_ANDROID_ATTACH:
            device_attached = TRUE;
            device_handle = data;
            return TRUE;

        default :
            break;
    }
    return FALSE;
}

/****************************************************************************
  Function:
    void InitPIC32(void)

  Summary:
    Initialize the PIC32 core to the correct modes and clock speeds

  Description:
    Initialize the PIC32 core to the correct modes and clock speeds

  Precondition:
    Only runs on PIC32

  Parameters:
    None

  Return Values:
    None

  Remarks:
    None
  ***************************************************************************/
#if defined(__PIC32MX__)
static void InitPIC32(void)
{
    int  value;

    #if defined(RUN_AT_60MHZ)
        // Use OSCCON default
    #else
        OSCCONCLR = 0x38000000; //PLLODIV
        #if defined(RUN_AT_48MHZ)
            OSCCONSET = 0x08000000; //PLLODIV /2
        #elif defined(RUN_AT_24MHZ)
            OSCCONSET = 0x10000000; //PLLODIV /4
        #else
            #error Cannot set OSCCON
        #endif
    #endif

    value = SYSTEMConfigWaitStatesAndPB( GetSystemClock() );

    // Enable the cache for the best performance
    CheKseg0CacheOn();

    INTEnableSystemMultiVectoredInt();

    DDPCONbits.JTAGEN = 0;

    value = OSCCON;
    while (!(value & 0x00000020))
    {
        value = OSCCON;    // Wait for PLL lock to stabilize
    }

    INTEnableInterrupts();
}
#endif