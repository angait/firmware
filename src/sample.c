#include "sample.h"

/*!
 * Retrieves a sample from a buffer.
 * @param queue Buffer from which a sample is removed.
 * @return The oldest sample in the buffer.
 */
inline SAMPLE SAMPLE_BUFFER_REMOVE(SAMPLE_BUFFER * queue) {
    SAMPLE e = queue->buffer[queue->read % 1024 ];
    ++queue->read;
    return e;
}

/*!
 * Add a sample to a buffer.
 * @param queue Buffer from which a sample is added.
 * @param element Sample to add to the buffer.
 */
inline void SAMPLE_BUFFER_ADD(SAMPLE_BUFFER *queue, SAMPLE element) {
    queue->buffer[queue->write % 1024 ] = element;
    ++queue->write;
}

/*!
 * Check if a buffer is empty.
 * \param queue Buffer to check.
 * \return 1 if the buffer is empty, 0 otherwise.
 */

inline short SAMPLE_BUFFER_EMPTY(SAMPLE_BUFFER *queue) {
    return queue->write == queue->read;
}

/*!
 * Check if a buffer is full.
 * \param queue Buffer to check.
 * \return 1 if the buffer is full, 0 otherwise.
 */

inline short SAMPLE_BUFFER_FULL(SAMPLE_BUFFER *queue) {
    return (queue->write != queue->read) && ((queue->write) % 1024 == (queue->read % 1024));
}

/*!
 * Initializes a buffer.
 * \param queue Buffer to initialize.
 */
void SAMPLE_BUFFER_INIT(SAMPLE_BUFFER *queue) {
    queue->read = 0;
    queue->write = 0;
}