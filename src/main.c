#include <stdint.h>
#include <GenericTypeDefs.h>
#include <Compiler.h>
#include <timer.h>
#include <USB/usb.h>
#include <USB/usb_host_android.h>

#include "DeviceConfiguration.h"
#include "sample.h"
#include "sensors.h"

/**********************/
/* Device information */
/**********************/
static char manufacturer[] = "University of Pittsburgh";
static char model[] = "angait";
static char description[] = "Angait: An Android Sampling Platform";
static char version[] = "0.1";
static char uri[] = "http://www.imedlab.org/";
static char serial[] = "N/A";

// Define device information
ANDROID_ACCESSORY_INFORMATION myDeviceInfo =
{
    manufacturer,
    sizeof(manufacturer),
    model,
    sizeof(model),
    description,
    sizeof(description),
    version,
    sizeof(version),
    uri,
    sizeof(uri),
    serial,
    sizeof(serial)
};


SAMPLE_BUFFER samples;         // Sample buffer
BOOL BUFFER_OVERFLOW = FALSE;  // Buffer overflow flag
BOOL INTERRUPT_MISSED = FALSE; // Missed interrupt flag (useful?)
ACCEL_AXIS X_AXIS, Y_AXIS, Z_AXIS, *currentAxis; // Accelerometer data structures
BOOL newSPIcmd = TRUE;

// Temporary variables used in interrupts
BYTE byte1, byte2, byte3;
SAMPLE s;

static void* device_handle = NULL;   // Pointer to the connected device
static BOOL device_attached = FALSE; // Device attachment flag

/*!
 * Structure containing the states of the state machine.
 */
typedef enum {SAMPLING, CLOSING, IDLE, WAITING,
              RESET, OVERFLOW, WRITING_SAMPLES} STATE;

STATE current_state, next_state;

/*!
 * Initializes the USB stack.
 */
void init_USBStack() {
    USBInitialize(0);
    AndroidAppStart(&myDeviceInfo);
}

/*!
 * Initializes the accelerometer data.
 */
void init_axis(void)
{
    /*
    X_AXIS.value = 0B11000000;
    //X_AXIS.value = 0B11010100;
    X_AXIS.next = &Y_AXIS;
    Y_AXIS.value = 0B11001000;
    //Y_AXIS.value = 0B11000000;
    Y_AXIS.next = &Z_AXIS;
    Z_AXIS.value = 0B11010000;
    //Z_AXIS.value = 0B11000000;
    Z_AXIS.next = &X_AXIS;
    */
    
    X_AXIS.value = 0x00;
    X_AXIS.next = &Y_AXIS;

    Y_AXIS.value = 0x40;
    Y_AXIS.next = &Z_AXIS;

    Z_AXIS.value = 0x80;
    Z_AXIS.next = &X_AXIS;
    
    currentAxis = &X_AXIS;
}

BOOL android_sent_command(const char command);

/*
 * Main function
 */
int main(void)
{
    /************************
     * Angait initilization *
     ************************/

    // Initialize the buffer
    SAMPLE_BUFFER_INIT(&samples);
    
    // Initialize USB Stack
    init_USBStack();

    // Initialize Timer3
    init_timer3();

    // Initialize ADC
    init_adc1();

    // Initialize SPI module
    // to communicate with the accelerometer
    init_accel();

    // Set pin RF5 as output (missed interrupt detection)
    //TRISFbits.TRISF5 = 0;

    // Initialize data structures
    init_axis();

    // Activate debugging
    DEBUG_Init(1);

    // Initialize state machine
    current_state = RESET;
    next_state = RESET;

    // Set pin RB12 as output (?)
    //TRISBbits.TRISB12 = 0;

    // Useful variables
    SAMPLE s;
    BUFFER_OVERFLOW = FALSE;
    //TODO bool closeSent = 0;

    int handledSamples = 0, sendTryCount = 0;
    BYTE sendArray[64]; // Array containing samples which have to be sent
                        // to the phone
    BYTE fillArray[64]; // Array where samples are written temporarily
    BYTE sendErrorCode;
    DWORD sendSize = 0;

    // Main loop
    for(;;)
    {   
        // Advance the state machine
        current_state = next_state;

        //Keep the USB stack running
        USBTasks();
        /*
        USBTasks();
        USBTasks();
        USBTasks();
        USBTasks();
        USBTasks();
        */

        switch(current_state)
        {
            // IDLE state
            case IDLE:
                if(device_attached /* && android_sent_toggle()*/)
                {
                    // TODO: add power saving state
                    next_state = WAITING;
                }
                break;

            // WAITING state
            case WAITING:
                if (device_attached)
                {
                    if(android_sent_command('x')) // If command 'x' is sent from
                                                  // the phone
                    {
                        // Enable the timer interrupt for sampling
                        // Initialize the sample buffer
                        SAMPLE_BUFFER_INIT(&samples);

                        // Desactivate flags
                        INTERRUPT_MISSED = FALSE;
                        BUFFER_OVERFLOW = FALSE;

                        newSPIcmd = TRUE;

                        // ???
                        int wait;
                        for(wait = 0; wait < INT_MAX; ++wait) USBTasks();

                        // Activate interrupts for SPI module and ADC
                        IEC0bits.SPI1IE = 1;
                        IEC0bits.AD1IE  = 1;

                        // Activate ADC and Timer3 modules
                        AD1CON1bits.ADON = 1;
                        T3CONbits.TON = 1;

                        // Go to SAMPLING state
                        next_state = SAMPLING;
                    }
                }
                else
                {
                    next_state = IDLE;
                }
                break;

            // SAMPLING state
            case SAMPLING:
                if (BUFFER_OVERFLOW || android_sent_command('s') || !device_attached)
                {
                    next_state = CLOSING;
                }
                /*
                else if (newSPIcmd)
                {
                    newSPIcmd = FALSE;

                    
                    SPI1BUF = 0x06;
                    SPI1BUF = currentAxis->value;
                    SPI1BUF = 0x00;
                    
                }
                 */
                else if (handledSamples >= 32)
                {
                    next_state = WRITING_SAMPLES;
                }
                else if (!SAMPLE_BUFFER_EMPTY(&samples))
                {
                    s = SAMPLE_BUFFER_REMOVE(&samples);
                    // Split the 16-bit value into two 8-bit values
                    fillArray[handledSamples*2] = (BYTE)(s.value>>8);
                    fillArray[handledSamples*2+1] = (BYTE)(s.value);
                    ++handledSamples;
                }
            
                break;

            // WRITING_SAMPLES state
            case WRITING_SAMPLES:
                if(AndroidAppIsWriteComplete(device_handle, &sendErrorCode, &sendSize))
                {
                    memcpy(sendArray,fillArray,64);
                    /* The fillArray must remain constant until
                     * AndroidAppIsWriteComplete. This is of course not documented
                     * in the pre/postconditions of the function*/
                    sendErrorCode = AndroidAppWrite(device_handle,sendArray,64);

                    if(sendErrorCode == USB_SUCCESS)
                    {
                        handledSamples = 0;
                        sendTryCount = 0;
                        next_state = SAMPLING;
                    }
                    else
                    {
                    /* succesfully called usb to send packet, so let's reset */
                    
                    }
                }
                break;
            
            // CLOSING state
            case CLOSING:
                //TODO: See android issue 20545 for the necessity of this.
                // TODO write a sample to the device
                T3CONbits.TON = 0;
                IEC0bits.AD1IE = 0;
                IEC0bits.SPI1IE = 0;
                newSPIcmd = FALSE;
                next_state = RESET;

                if (!device_attached) // No need to work around if device was disconnected
                {
                    next_state = RESET;
                }
                else if (AndroidAppIsWriteComplete(device_handle, &sendErrorCode, &sendSize))
                {
                    fillArray[0] = 'c';
                    memcpy(sendArray,fillArray,1);
                    sendErrorCode = AndroidAppWrite(device_handle,fillArray,1);
                
                    if(sendErrorCode == USB_SUCCESS)
                    {
                        next_state = RESET;
                    } else {
                    /* succesfully called usb to send packet, so let's reset */
                    }
                }

                sendTryCount++;
                if (sendTryCount > 255)
                {
                    next_state = RESET;
                }
                break;

            // OVERFLOW state
            case OVERFLOW:
                next_state = RESET;

//            if(android_sent_command('x') || !device_attached)
//            {
//                IEC0bits.AD1IE = 0;
//                next_state = CLOSING;
//            }
                break;
        
            // RESET state
            case RESET:
                DEBUG_PrintString("Entering RESET");
            
                // TODO: figure this out. I think it may require some android hacks
                U1CONbits.USBRST = 1;
                DelayMs(50);
                U1CONbits.USBRST = 0;
                DelayMs(10);
                init_USBStack();
                U1CONbits.USBRST = 1;
                DelayMs(50);
                U1CONbits.USBRST = 0;
                DelayMs(10);
                init_USBStack();
                DelayMs(50);
                U1CONbits.USBRST = 0;
                DelayMs(10);
                init_USBStack();

                next_state = IDLE;
                // Disable the timer interrupt for sampling
                AD1CON1bits.ADON = 0;
                T3CONbits.TON = 0;
                sendTryCount = 0;
                handledSamples = 0;
            break;
        } // End of switch(current_state)
    } // End of main loop
} // End of main function


/********
 * ISRs *
 ********/

// C30 Exception Handlers
// If your code gets here, you either tried to read or write
// a NULL pointer, or your application overflowed the stack
// by having too many local variables or parameters declared.
void _ISR __attribute__((__no_auto_psv__)) _AddressError(void)
{
    while(1){}
}
void _ISR __attribute__((__no_auto_psv__)) _StackError(void)
{
    while(1){}
}

/*
 * ADC's interrupt service routine
 */
void __attribute__ ((__interrupt__, no_auto_psv)) _ADC1Interrupt(void)
{
    SAMPLE s;
    static BOOL INTERRUPT_TOGGLE = FALSE;
    static BOOL ENTERED_INTERRUPT;

    /* Ensure the accuracy of sampling by checking for missed interrupts, dangerous!*/
    INTERRUPT_TOGGLE = !INTERRUPT_TOGGLE;
    LATFbits.LATF5 = INTERRUPT_TOGGLE;

    if(ENTERED_INTERRUPT) {
        INTERRUPT_MISSED = TRUE;
    } else {
        IFS0bits.AD1IF = 0;
        ENTERED_INTERRUPT = TRUE;
    }

    static int i;
    for (i = 0; i < 7; ++i)
    {
        s.channel = i;
        s.value = (&ADC1BUF0)[i+AD1CON2bits.BUFS*8];
//        s.value = i+1;

        if (!SAMPLE_BUFFER_FULL(&samples)) {
            SAMPLE_BUFFER_ADD(&samples, s);
        } else {
            /* TODO document that sometimes dalvik cache must be cleared for this to work */
            BUFFER_OVERFLOW = TRUE;
        }
    }

    /* Send three bytes out of SPI1 to initiate communication with H48C */
    //LATDbits.LATD11 = 0; // Set slave select low

    // Write the command to get a measurement from the accelerometer
    /*
    SPI1BUF = currentAxis->value;
    
    SPI1BUF = 0;
    SPI1BUF = 0;
    */
    
    SPI1BUF = 0x06;
    SPI1BUF = currentAxis->value;
    SPI1BUF = 0x00;
    
    // Change axis
    //currentAxis = currentAxis->next;

    /* End missed interrupt detection */
    ENTERED_INTERRUPT = FALSE;
}


/*
 * SPI module's interrupt service routine
 */
void __attribute__ ((__interrupt__, no_auto_psv)) _SPI1Interrupt(void)
{
    //LATDbits.LATD11 = 1; // Set slave select high

    byte1 = 0;
    byte2 = 0;
    byte3 = 0;
    //static BYTE byte3;

    s.channel = 0;
    s.value = 0;

    //LATAbits.LATA15 = 1;
    /*
    byte1 = SPI1BUF;
    byte2 = SPI1BUF;
    byte3 = SPI1BUF;
    */
    /*TODO check the count register for SPI1BUF. The above approach may be dangerous */

    /* Add sample to buffer */
    s.channel = (currentAxis->value & 0B111)+16;
    //s.channel = (UINT8) currentAxis->value;
    //s.value = ((uint16_t)(byte1<<7)<<4)|(uint16_t)byte2<<3|(uint16_t)(byte3>>5);
    
    byte1 = SPI1BUF; // Read most recent buffer

    byte2 = SPI1BUF;
    byte2 &= 0x0F; // We care about the last 4 bits only

    byte3 = SPI1BUF; // Last buffer discarded

    // Generate the sample
    s.value |= (UINT16)(byte2) << 8;
    s.value |= (UINT16)byte1;
    
    if (!SAMPLE_BUFFER_FULL(&samples)) {
        SAMPLE_BUFFER_ADD(&samples, s);
        newSPIcmd = TRUE;
        currentAxis = currentAxis->next;
    } else {
        /* TODO document that sometimes dalvik cache must be cleared for this to work */
        BUFFER_OVERFLOW = TRUE;
        newSPIcmd = FALSE;
    }
    
    // Clear interrupt flag
    IFS0bits.SPI1IF = 0;
}


/************************
 * Device communication *
 ************************/

/* android_sent_toggle()
 * Precondition: android device is attached for reading
 * Returns: true upon a toggle command being sent from the android phone
 */

inline BOOL android_sent_command(const char command) {
    static BYTE errorCode;
    static DWORD size = 0;
    static BOOL toggleSent;
    static BYTE read_buffer[64];

    toggleSent = FALSE;

    if(AndroidAppIsReadComplete(device_handle, &errorCode, &size))
    {
        errorCode = AndroidAppRead(device_handle, (BYTE*)&read_buffer, (DWORD)sizeof(read_buffer));
        // If the device is attached, then lets wait for a command from the application
        if( errorCode != USB_SUCCESS)
        {
            DEBUG_PrintString("Error trying to start read");
        }
        else if (size > 0 && read_buffer[0] == command)
        {
            toggleSent = TRUE;
        }
    }

    return toggleSent;
}

/****************************************************************************
  Function:
    BOOL USB_ApplicationDataEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )

  Summary:
    Handles USB data application events

  Description:
    Handles USB data application events

  Precondition:
    None

  Parameters:
    BYTE address - address of the device causing the event
    USB_EVENT event - the event that has occurred
    void* data - data associated with the event
    DWORD size - the size of the data in the data field

  Return Values:
    BOOL - Return TRUE of the event was processed.  Return FALSE if the event
           wasn't handled.

  Remarks:
    None
  ***************************************************************************/
BOOL USB_ApplicationDataEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )
{
    return FALSE;
}


/****************************************************************************
  Function:
    BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )

  Summary:
    Handles USB application events

  Description:
    Handles USB application events

  Precondition:
    None

  Parameters:
    BYTE address - address of the device causing the event
    USB_EVENT event - the event that has occurred
    void* data - data associated with the event
    DWORD size - the size of the data in the data field

  Return Values:
    BOOL - Return TRUE of the event was processed.  Return FALSE if the event
           wasn't handled.

  Remarks:
    None
  ***************************************************************************/
BOOL USB_ApplicationEventHandler( BYTE address, USB_EVENT event, void *data, DWORD size )
{
    switch( (INT)event )
    {
        case EVENT_VBUS_REQUEST_POWER:
            // The data pointer points to a byte that represents the amount of power
            // requested in mA, divided by two.  If the device wants too much power,
            // we reject it.
            if (((USB_VBUS_POWER_EVENT_DATA*)data)->current <= (MAX_ALLOWED_CURRENT / 2))
            {
                return TRUE;
            }
            else
            {
                DEBUG_PrintString( "\r\n***** USB Error - device requires too much current *****\r\n" );
            }
            break;

        case EVENT_VBUS_RELEASE_POWER:
        case EVENT_HUB_ATTACH:
        case EVENT_UNSUPPORTED_DEVICE:
        case EVENT_CANNOT_ENUMERATE:
        case EVENT_CLIENT_INIT_ERROR:
        case EVENT_OUT_OF_MEMORY:
        case EVENT_UNSPECIFIED_ERROR:   // This should never be generated.
        case EVENT_DETACH:                   // USB cable has been detached (data: BYTE, address of device)
        case EVENT_ANDROID_DETACH:
            device_attached = FALSE;
            return TRUE;
            break;

        // Android Specific events
        case EVENT_ANDROID_ATTACH:
            device_attached = TRUE;
            device_handle = data;
            return TRUE;

        default :
            break;
    }
    return FALSE;
}