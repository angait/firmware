/* 
 * File:   sensors.h
 * Author: Alexandre Millecamps
 * Description:
 */

#ifndef SENSORS_H
#define	SENSORS_H

#include <GenericTypeDefs.h>

/*!
 * Structure encapsulating the value of an acceleration along an axis
 * and a pointer to a another acceleration axis to measure.
 */
typedef struct _ACCEL_AXIS {
    BYTE value;
    //INT16 value;
    struct _ACCEL_AXIS *next;
} ACCEL_AXIS;

//void init_axis(void);

#endif	/* SENSORS_H */

