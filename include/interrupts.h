/* 
 * File:   interrupts.h
 * Author: Alexandre Millecamps
 *
 * Interrupt service routines (ISR) are implemented in this file.
 *
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

// C30 Exception Handlers
// If your code gets here, you either tried to read or write
// a NULL pointer, or your application overflowed the stack
// by having too many local variables or parameters declared.
void _ISR __attribute__((__no_auto_psv__)) _AddressError(void)
{
    while(1){}
}
void _ISR __attribute__((__no_auto_psv__)) _StackError(void)
{
    while(1){}
}

/*
 * ADC's interrupt service routine
 */
void __attribute__ ((__interrupt__, no_auto_psv)) _ADC1Interrupt(void)
{
    static SAMPLE s;
    static BOOL INTERRUPT_TOGGLE = FALSE;
    static BOOL ENTERED_INTERRUPT;

    /* Ensure the accuracy of sampling by checking for missed interrupts, dangerous!*/
    INTERRUPT_TOGGLE = !INTERRUPT_TOGGLE;
    LATFbits.LATF5 = INTERRUPT_TOGGLE;

    if(ENTERED_INTERRUPT) {
        INTERRUPT_MISSED = TRUE;
    } else {
        IFS0bits.AD1IF = 0;
        ENTERED_INTERRUPT = TRUE;
    }

    static int i;
    for (i = 0; i < 7; ++i)
    {
        s.channel = i;
        s.value = (&ADC1BUF0)[i+AD1CON2bits.BUFS*8];
//        s.value = i+1;

        if (!SAMPLE_BUFFER_FULL(&samples)) {
            SAMPLE_BUFFER_ADD(&samples, s);
        } else {
            /* TODO document that sometimes dalvik cache must be cleared for this to work */
            BUFFER_OVERFLOW = TRUE;
        }
    }

    /* Send three bytes out of SPI1 to initiate communication with H48C */
    LATAbits.LATA15 = 0; // Set slave select low

    SPI1BUF = currentAxis->value;
    SPI1BUF = 0;
    SPI1BUF = 0;

    /* End missed interrupt detection */
    ENTERED_INTERRUPT = FALSE;
}


/*
 * SPI module's interrupt service routine
 */
void __attribute__ ((__interrupt__, no_auto_psv)) _SPI1Interrupt(void)
{
    static BYTE byte1;
    static BYTE byte2;
    static BYTE byte3;
    static SAMPLE s;

    LATAbits.LATA15 = 1;
    byte1 = SPI1BUF;
    byte2 = SPI1BUF;
    byte3 = SPI1BUF;
    /*TODO check the count register for SPI1BUF. The above approach may be dangerous */

    /* Add sample to device*/
    s.channel = (currentAxis->value & 0B111)+16;
    s.value = ((uint16_t)(byte1<<7)<<4)|(uint16_t)byte2<<3|(uint16_t)(byte3>>5);
//    s.value = 0;

    if (!SAMPLE_BUFFER_FULL(&samples)) {
        SAMPLE_BUFFER_ADD(&samples, s);
    } else {
        /* TODO document that sometimes dalvik cache must be cleared for this to work */
        BUFFER_OVERFLOW = TRUE;
    }

    currentAxis = currentAxis->next;
    IFS0bits.SPI1IF = 0;
}

#endif	/* INTERRUPTS_H */

